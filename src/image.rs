use crate::{config::CONFIG, vertex::Vertex, Result};
use glium::{
    index::{NoIndices, PrimitiveType::TriangleStrip},
    texture::{RawImage2d, SrgbTexture2d},
    uniform,
    uniforms::SamplerWrapFunction::Clamp,
    Blend, Display, DrawParameters, Program, Surface, VertexBuffer,
};
use std::{collections::HashSet, path::PathBuf};

pub struct Image {
    pub position: (u32, u32),
    f_position: (f32, f32),
    pub path: PathBuf,
    pub tags: HashSet<String>,
    pub selected: bool,

    ratio: (f32, f32),
    vertex_buffer: VertexBuffer<Vertex>,
    indices: NoIndices,
    program: Program,
    texture: SrgbTexture2d,
}

impl Image {
    pub fn new(
        display: &Display,
        image: &[u8],
        size: (u32, u32),
        path: PathBuf,
        tags: HashSet<String>,
    ) -> Result<Image> {
        let ratio = if size.0 > size.1 {
            (1.0, size.1 as f32 / size.0 as f32)
        } else {
            (size.0 as f32 / size.1 as f32, 1.0)
        };
        Ok(Image {
            position: (0, 0),
            f_position: (0.0, 0.0),
            path,
            tags,
            selected: false,
            ratio,
            vertex_buffer: VertexBuffer::new(
                display,
                &[
                    Vertex::new(-ratio.0, -ratio.1, 0.0, 0.0),
                    Vertex::new(-ratio.0, ratio.1, 0.0, 1.0),
                    Vertex::new(ratio.0, -ratio.1, 1.0, 0.0),
                    Vertex::new(ratio.0, ratio.1, 1.0, 1.0),
                ],
            )?,
            indices: NoIndices(TriangleStrip),
            program: Program::from_source(
                display,
                include_str!("shaders/cell.vert"),
                include_str!("shaders/image.frag"),
                None,
            )?,
            texture: {
                let raw = RawImage2d::from_raw_rgba_reversed(&image, size);
                SrgbTexture2d::new(display, raw)?
            },
        })
    }

    pub fn select(&mut self) {
        self.selected = !self.selected;
    }

    pub fn update(&mut self) {
        self.f_position = (
            self.f_position.0 + (self.position.0 as f32 - self.f_position.0) * 0.25,
            self.f_position.1 + (self.position.1 as f32 - self.f_position.1) * 0.25,
        );
    }

    pub fn draw(
        &mut self,
        target: &mut impl Surface,
        scroll: f32,
        offset: f32,
        image_size: (f32, f32),
        cell_size: (f32, f32),
    ) -> Result<()> {
        target.draw(
            &self.vertex_buffer,
            &self.indices,
            &self.program,
            &uniform! {
                position: if CONFIG.animation { self.f_position } else { (self.position.0 as f32, self.position.1 as f32) },
                scroll: scroll,
                offset: offset,
                image_size: image_size,
                cell_size: cell_size,
                tex: self.texture.sampled().wrap_function(Clamp),
                selected: self.selected,
                cursor_width: CONFIG.cursor_width,
                marker_size: CONFIG.marker_size,
                marker_color: CONFIG.marker_color,
                pixel_size: 1.0 / CONFIG.image_size as f32,
                ratio: self.ratio,
            },
            &DrawParameters {
                blend: Blend::alpha_blending(),
                ..Default::default()
            },
        )?;
        Ok(())
    }
}
