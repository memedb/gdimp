use serde::{de::Error, Deserialize, Deserializer, Serializer};

type Hex = (f32, f32, f32, f32);
type IntHex = (u32, u32, u32, u32);

pub fn serialize<S>(color: &Hex, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    serializer.serialize_str(&to_hex(&(
        (color.0 * 255.0) as u32,
        (color.1 * 255.0) as u32,
        (color.2 * 255.0) as u32,
        (color.3 * 255.0) as u32,
    )))
}

fn to_hex(color: &IntHex) -> String {
    let number = ((color.0) << 24) + ((color.1) << 16) + ((color.2) << 8) + (color.3);
    format!("#{:08X}", number)
}

pub fn deserialize<'de, D>(deserializer: D) -> Result<Hex, D::Error>
where
    D: Deserializer<'de>,
{
    let color: String = Deserialize::deserialize(deserializer)?;
    let hex = from_hex(&color).map_err(Error::custom)?;
    Ok((
        hex.0 as f32 / 255.0,
        hex.1 as f32 / 255.0,
        hex.2 as f32 / 255.0,
        hex.3 as f32 / 255.0,
    ))
}

fn from_hex(color: &str) -> crate::Result<IntHex> {
    if color.len() != 9 || !color.starts_with('#') {
        Err(format!("{} is not a valid hexadecimal color", color).into())
    } else {
        let color = u32::from_str_radix(color.trim_start_matches('#'), 16)?;
        let r = (color >> 24) & 0xFF;
        let g = (color >> 16) & 0xFF;
        let b = (color >> 8) & 0xFF;
        let a = color & 0xFF;
        Ok((r, g, b, a))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from_hex() {
        assert_eq!(from_hex("#FFFFFFFF").unwrap(), (255, 255, 255, 255));
        assert_eq!(from_hex("#00000000").unwrap(), (0, 0, 0, 0));
    }

    #[test]
    fn test_to_hex() {
        assert_eq!(to_hex(&(255, 255, 255, 255)), "#FFFFFFFF");
        assert_eq!(to_hex(&(0, 0, 0, 0)), "#00000000");
    }

    #[test]
    fn test_consistency() {
        for color in &[(255, 255, 255, 255), (127, 127, 127, 127), (0, 0, 0, 0)] {
            assert_eq!(from_hex(&to_hex(color)).unwrap(), *color);
        }
    }
}
