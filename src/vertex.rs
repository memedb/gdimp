#![allow(clippy::unneeded_field_pattern)]

use glium::implement_vertex;

#[derive(Copy, Clone)]
pub struct Vertex {
    v_position: [f32; 2],
    v_uv: [f32; 2],
}

impl Vertex {
    pub fn new(x: f32, y: f32, u: f32, v: f32) -> Vertex {
        Vertex {
            v_position: [x, y],
            v_uv: [u, v],
        }
    }
}
implement_vertex!(Vertex, v_position, v_uv);
