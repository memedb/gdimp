use crate::config::CONFIG;

#[derive(Default)]
pub struct LayoutData {
    pub offset: f32,
    pub cells: (u32, u32),
    pub cell_size: (f32, f32),
    pub image_size: (f32, f32),
}

pub fn calculate_layout_data(line_height: f32, window_size: (u32, u32)) -> LayoutData {
    let offset = line_height * 2.0 / window_size.1 as f32;

    let image_size = (
        CONFIG.image_size as f32 / window_size.0 as f32,
        CONFIG.image_size as f32 / window_size.1 as f32,
    );

    let cells = (
        window_size.0 / (CONFIG.image_size + CONFIG.gap).max(1),
        (window_size.1 - line_height as u32 * 2) / (CONFIG.image_size + CONFIG.gap).max(1),
    );

    let cell_size = (2.0 / cells.0 as f32, (2.0 - offset) / cells.1 as f32);

    LayoutData {
        offset,
        cells,
        cell_size,
        image_size,
    }
}
