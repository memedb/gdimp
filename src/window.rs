use crate::{
    cache::Cache,
    config::CONFIG,
    cursor::Cursor,
    image::Image,
    layout::{calculate_layout_data, LayoutData},
    output::output,
    Result,
};
use font_loader::system_fonts;
use glium::{
    glutin::{
        event::{
            DeviceEvent::*, ElementState::Pressed, Event, StartCause::*, VirtualKeyCode::*,
            WindowEvent::*,
        },
        event_loop::{ControlFlow, EventLoop},
        window::WindowBuilder,
        ContextBuilder,
    },
    Display, Surface,
};
use glium_glyph::{
    glyph_brush::{
        rusttype::{Font, Scale},
        HorizontalAlign, Layout, Section,
    },
    GlyphBrush,
};
use memedb_core::write_tags;

const TITLE: &str = "Graphic Design Is My Passion";

enum State {
    Browse,
    Search,
    Add,
    Remove,
    Toggle,
}

pub struct Window<'a> {
    display: Display,
    glyph_brush: GlyphBrush<'a, 'a>,
    cursor: Cursor,

    line_height: f32,

    layout_data: LayoutData,

    ctrl: bool, // THIS IS STUPID

    line: String,
    state: State,
    buffer: String,
    query: String,
    current: usize,
    scroll: u32,
    f_scroll: f32,

    images: Vec<Image>,
    filtered: Vec<usize>,
}

impl<'a> Window<'a> {
    pub fn images_from_cache(&mut self, cache: Cache) -> Result<()> {
        self.images = cache
            .data
            .into_iter()
            .map(|(p, f)| Image::new(&self.display, &f.thumbnail, f.size, p, f.tags))
            .collect::<Result<_>>()?;

        self.images.sort_unstable_by(|a, b| a.path.cmp(&b.path));

        self.filtered = (0..self.images.len()).collect();

        Ok(())
    }

    pub fn new(cache: Cache, event_loop: &EventLoop<()>) -> Result<Window<'a>> {
        let wb = WindowBuilder::new()
            .with_transparent((CONFIG.background_color.3 - 1.0).abs() > std::f32::EPSILON)
            .with_title(TITLE);
        let cb = ContextBuilder::new();
        let display = Display::new(wb, cb, &event_loop)?;

        let property = system_fonts::FontPropertyBuilder::new()
            .family(&CONFIG.font_family)
            .build();
        let (font, _) = system_fonts::get(&property).ok_or("Couldn't find font")?;
        let font = Font::from_bytes(font)?;
        let v_metrics = font.v_metrics(Scale::uniform(CONFIG.font_size));
        let line_height = v_metrics.ascent.abs() + v_metrics.descent.abs();
        let glyph_brush = GlyphBrush::new(&display, vec![font]);
        let cursor = Cursor::new(&display)?;

        let mut window = Window {
            display,
            glyph_brush,
            cursor,

            line_height,

            layout_data: Default::default(),

            ctrl: false,

            line: String::from(TITLE),
            state: State::Browse,
            buffer: String::new(),
            query: String::new(),
            current: 0,
            scroll: 0,
            f_scroll: 0.0,

            images: Vec::new(),
            filtered: Vec::new(),
        };

        window.images_from_cache(cache)?;
        window.calculate_layout();
        window.update_line();

        Ok(window)
    }

    fn update_line(&mut self) {
        self.line = match self.state {
            State::Browse => match self.filtered.get(self.current).map(|i| &self.images[*i]) {
                Some(img) => format!(
                    "{}{}{}",
                    img.path.file_name().unwrap().to_string_lossy(),
                    if img.tags.is_empty() { "" } else { ": " },
                    img.tags.iter().cloned().collect::<Vec<_>>().join(",")
                ),
                None => String::from(TITLE),
            },
            State::Search => format!("Searching for: {}", self.query),
            State::Add => format!("Add tag: {}", self.buffer),
            State::Remove => format!("Remove tag: {}", self.buffer),
            State::Toggle => format!("Toggle tag: {}", self.buffer),
        }
    }

    fn set_state(&mut self, state: State) -> bool {
        self.state = state;
        self.buffer.clear();
        self.update_line();
        true
    }

    pub fn calculate_layout(&mut self) {
        self.layout_data =
            calculate_layout_data(self.line_height, self.display.get_framebuffer_dimensions());
        self.arrange_images();
    }

    fn arrange_images(&mut self) {
        let (columns, _) = self.layout_data.cells;
        for (i, &j) in self.filtered.iter().enumerate() {
            self.images[j].position = ((i as u32 % columns), (i as u32 / columns));
        }
        self.cursor.position = (self.current as u32 % columns, self.current as u32 / columns);
    }

    fn filter_images(&mut self) {
        self.update_line();

        if self.query.is_empty() {
            self.filtered = (0..self.images.len()).collect();
        } else {
            let tags: Vec<&str> = self.query.trim().trim_matches(',').split(',').collect();
            self.filtered = self
                .images
                .iter()
                .enumerate()
                .filter(|(_, img)| tags.iter().all(|q| img.tags.iter().any(|t| t.contains(q))))
                .map(|(i, _)| i)
                .collect();
        }

        self.current = 0;
        self.cursor.position = (0, 0);

        self.arrange_images();
        self.handle_scroll();
    }

    fn handle_move(&mut self, offset: i32) -> bool {
        if !self.filtered.is_empty() {
            let new_sel = self.current as i32 + offset;
            self.current = new_sel.rem_euclid(self.filtered.len() as i32) as usize;
            self.cursor.position = (
                self.current as u32 % self.layout_data.cells.0,
                self.current as u32 / self.layout_data.cells.0,
            );
        }

        self.update_line();
        self.handle_scroll();
        true
    }

    fn handle_scroll(&mut self) {
        let selected_y = self.current as u32 / self.layout_data.cells.0;
        if selected_y > self.scroll + self.layout_data.cells.1 - 1 {
            self.scroll = selected_y - self.layout_data.cells.1 + 1;
        } else if selected_y < self.scroll {
            self.scroll = selected_y;
        }
    }

    pub fn handle_event(
        &mut self,
        event: Event<()>,
        control_flow: &mut ControlFlow,
    ) -> Result<bool> {
        Ok(match event {
            Event::DeviceEvent { event, .. } => match event {
                ModifiersChanged(state) => {
                    self.ctrl = state.ctrl();
                    false
                }
                _ => false,
            },
            Event::RedrawRequested(_) => true,
            Event::WindowEvent { event, .. } => match event {
                CloseRequested => {
                    *control_flow = ControlFlow::Exit;
                    false
                }
                Resized(_) => {
                    self.calculate_layout();
                    true
                }
                KeyboardInput { input, .. } if input.state == Pressed => {
                    match input.virtual_keycode {
                        Some(key) => match self.state {
                            State::Browse => match key {
                                Up => self.handle_move(-(self.layout_data.cells.0 as i32)),
                                Right => self.handle_move(1),
                                Down => self.handle_move(self.layout_data.cells.0 as i32),
                                Left => self.handle_move(-1),
                                Space => {
                                    if !self.filtered.is_empty() {
                                        self.images[self.filtered[self.current]].select();
                                        self.handle_move(1)
                                    } else {
                                        false
                                    }
                                }
                                Escape => {
                                    *control_flow = ControlFlow::Exit;
                                    false
                                }
                                Return => {
                                    *control_flow = ControlFlow::Exit;
                                    if !self.filtered.is_empty() {
                                        output(&self.images[self.filtered[self.current]].path)?;
                                    }
                                    false
                                }
                                // Holy shit i just realized the letters spell out fart
                                // I swear it wasnt on purpose
                                F if self.ctrl => self.set_state(State::Search),
                                A if self.ctrl => self.set_state(State::Add),
                                R if self.ctrl => self.set_state(State::Remove),
                                T if self.ctrl => self.set_state(State::Toggle),
                                _ => false,
                            },
                            State::Search => match key {
                                Back => {
                                    self.query.pop();
                                    self.filter_images();
                                    true
                                }
                                F if self.ctrl => self.set_state(State::Browse),
                                Escape | Return => self.set_state(State::Browse),
                                _ => false,
                            },
                            State::Add | State::Remove | State::Toggle => match key {
                                Back => {
                                    self.buffer.pop();
                                    self.update_line();
                                    true
                                }
                                Escape => self.set_state(State::Browse),
                                Return => {
                                    for img in self.images.iter_mut().filter(|img| img.selected) {
                                        match self.state {
                                            State::Add => img.tags.insert(self.buffer.clone()),
                                            State::Remove => img.tags.remove(&self.buffer),
                                            State::Toggle => {
                                                if img.tags.contains(&self.buffer) {
                                                    img.tags.remove(&self.buffer)
                                                } else {
                                                    img.tags.insert(self.buffer.clone())
                                                }
                                            }
                                            _ => true,
                                        };
                                        write_tags(&img.path, &img.tags)?;
                                    }
                                    self.filter_images();
                                    self.set_state(State::Browse)
                                }
                                _ => false,
                            },
                        },
                        None => false,
                    }
                }
                ReceivedCharacter(ch) => match self.state {
                    State::Search if !ch.is_control() => {
                        self.query.push(ch);
                        self.filter_images();
                        true
                    }
                    State::Add | State::Remove | State::Toggle if !ch.is_control() => {
                        self.buffer.push(ch);
                        self.update_line();
                        true
                    }
                    _ => false,
                },
                _ => false,
            },
            Event::NewEvents(cause) => match cause {
                Init => true,
                _ => false,
            },
            _ => false,
        })
    }

    pub fn draw(&mut self) -> Result<()> {
        if CONFIG.animation {
            self.f_scroll = self.f_scroll + (self.scroll as f32 - self.f_scroll) * 0.25;
        }

        let mut target = self.display.draw();
        target.clear_color(
            CONFIG.background_color.0,
            CONFIG.background_color.1,
            CONFIG.background_color.2,
            CONFIG.background_color.3,
        );

        for (i, &j) in self.filtered.iter().enumerate() {
            self.images[j].update();
            let y = i as u32 / self.layout_data.cells.0;
            if y >= self.scroll && y < self.scroll + self.layout_data.cells.1 {
                self.images[j].draw(
                    &mut target,
                    if CONFIG.animation {
                        self.f_scroll
                    } else {
                        self.scroll as f32
                    },
                    self.layout_data.offset,
                    self.layout_data.image_size,
                    self.layout_data.cell_size,
                )?;
            }
        }

        self.cursor.draw(
            &mut target,
            if CONFIG.animation {
                self.f_scroll
            } else {
                self.scroll as f32
            },
            self.layout_data.offset,
            self.layout_data.image_size,
            self.layout_data.cell_size,
        )?;

        self.glyph_brush.queue(Section {
            color: [
                CONFIG.text_color.0,
                CONFIG.text_color.1,
                CONFIG.text_color.2,
                CONFIG.text_color.3,
            ],
            text: &self.line,
            scale: Scale::uniform(CONFIG.font_size),
            screen_position: (
                self.display.get_framebuffer_dimensions().0 as f32 / 2.0,
                0.0,
            ),
            layout: Layout::default().h_align(HorizontalAlign::Center),
            ..Default::default()
        });
        self.glyph_brush.draw_queued(&self.display, &mut target);

        target.finish()?;
        Ok(())
    }
}
