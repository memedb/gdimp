use crate::{hex, Result};
use dirs::config_dir;
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use std::{
    fs::{read, write},
    path::PathBuf,
};
use tinyfiledialogs::select_folder_dialog;
use toml::{from_slice, to_vec};

lazy_static! {
    pub static ref CONFIG: Config = get_config().unwrap();
}

#[derive(Deserialize, Serialize)]
pub struct Config {
    pub animation: bool,
    pub gap: u32,
    pub image_size: u32,
    pub font_family: String,
    pub font_size: f32,
    #[serde(with = "hex")]
    pub background_color: (f32, f32, f32, f32),
    #[serde(with = "hex")]
    pub text_color: (f32, f32, f32, f32),
    #[serde(with = "hex")]
    pub cursor_color: (f32, f32, f32, f32),
    pub cursor_width: u32,
    #[serde(with = "hex")]
    pub marker_color: (f32, f32, f32, f32),
    pub marker_size: u32,
    pub meme_folder: PathBuf,
    pub file_handler: Option<String>,
}

impl Default for Config {
    fn default() -> Self {
        Config {
            animation: true,
            gap: 10,
            image_size: 100,
            font_family: String::from("Arial"),
            font_size: 22.0,
            background_color: (0.047058824, 0.047058824, 0.17254902, 0.6666667),
            text_color: (0.7176471, 0.8352941, 0.9607843, 1.0),
            cursor_color: (0.40784314, 0.5803922, 0.9137255, 1.0),
            cursor_width: 5,
            marker_color: (0.7176471, 0.8352941, 0.9607843, 1.0),
            marker_size: 10,
            meme_folder: select_folder_dialog("Where are your memes?", ".")
                .map(PathBuf::from)
                .unwrap_or(PathBuf::from(".")),
            file_handler: None,
        }
    }
}

fn get_config() -> Result<Config> {
    let mut path = config_dir().ok_or("Couldn't find configuration directory")?;
    path.push("gdimp.toml");
    let mut config = if path.exists() {
        let bytes = read(path)?;
        from_slice(&bytes)?
    } else {
        let config = Config::default();
        write(path, to_vec(&config)?)?;
        config
    };
    config.background_color = gamma_correct(config.background_color);
    config.text_color = gamma_correct(config.text_color);
    config.cursor_color = gamma_correct(config.cursor_color);
    config.marker_color = gamma_correct(config.marker_color);
    Ok(config)
}

fn gamma_correct(color: (f32, f32, f32, f32)) -> (f32, f32, f32, f32) {
    (
        color.0.powf(2.2),
        color.1.powf(2.2),
        color.2.powf(2.2),
        color.3.powf(2.2),
    )
}
