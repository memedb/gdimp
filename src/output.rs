use crate::{Result, CONFIG};
use std::{path::PathBuf, process::Command};

#[cfg(target_os = "linux")]
const NOTFOUND_ERR: &str =
    "Couldn't find a file handler, please add one in the config file via the `file_handler` option";

#[cfg(target_os = "linux")]
const LINUX_OPENERS: &[(&str, &[&str])] =
    &[("dolphin", &["--select"]), ("nautilus", &["--select"])];

#[cfg(target_os = "linux")]
pub fn output(path: &PathBuf) -> Result<()> {
    if let Some(file_handler) = &CONFIG.file_handler {
        Command::new(file_handler).arg(path).spawn()?;
        Ok(())
    } else {
        for opener in LINUX_OPENERS {
            let command = Command::new(opener.0).args(opener.1).arg(path).spawn();
            if command.is_ok() {
                return Ok(());
            }
        }
        Err(NOTFOUND_ERR.into())
    }
}

#[cfg(target_os = "windows")]
pub fn output(path: &PathBuf) -> Result<()> {
    if let Some(file_handler) = &CONFIG.file_handler {
        Command::new(file_handler).arg(path).spawn()?;
    } else {
        Command::new("explorer.exe")
            .arg(format!("/select,\"{}\"", path.display()))
            .spawn()?;
    }
    Ok(())
}

#[cfg(all(not(target_os = "windows"), not(target_os = "linux")))]
pub fn output(path: &PathBuf) -> Result<()> {
    println!("{}", path.display());
    Ok(())
}
