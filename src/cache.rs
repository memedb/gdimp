use crate::{config::CONFIG, Result};
use bincode::{deserialize, serialize};
use dirs::cache_dir;
use image::open;
use memedb_core::read_tags;
use rayon::prelude::*;
use serde::{Deserialize, Serialize};
use std::{
    collections::{HashMap, HashSet},
    fs::File,
    io::{Read, Write},
    path::PathBuf,
    sync::mpsc::{channel, Receiver},
    thread::spawn,
    time::SystemTime,
};

#[derive(Deserialize, Serialize, Clone)]
pub struct Data {
    pub mtime: SystemTime,
    pub tags: HashSet<String>,
    pub size: (u32, u32),
    pub thumbnail: Vec<u8>,
}

#[derive(Deserialize, Serialize, Clone)]
pub struct Cache {
    pub image_size: u32,
    pub data: HashMap<PathBuf, Data>,
}

pub fn get_cache() -> (Result<Cache>, Option<Receiver<Cache>>) {
    if let Ok(old_cache) = old_cache() {
        let cloned_cache = old_cache.clone();
        let (sender, receiver) = channel();
        spawn(move || {
            if let Ok(new_cache) = new_cache(Some(cloned_cache)) {
                sender.send(new_cache).unwrap();
            }
        });
        (Ok(old_cache), Some(receiver))
    } else {
        (new_cache(None), None)
    }
}

fn old_cache() -> Result<Cache> {
    let mut file = cache_dir().ok_or("Couldn't find cache directory")?;
    file.push("gdimp_cache");

    let mut bytes = Vec::new();
    File::open(file)?.read_to_end(&mut bytes)?;

    let cache: Cache = deserialize(&bytes)?;
    Ok(cache)
}

fn new_cache(old_cache: Option<Cache>) -> Result<Cache> {
    let mut cache = match old_cache {
        Some(old_cache) if old_cache.image_size == CONFIG.image_size => old_cache,
        _ => Cache {
            image_size: CONFIG.image_size,
            data: HashMap::new(),
        },
    };

    let entries: HashSet<_> = CONFIG
        .meme_folder
        .read_dir()?
        .map(|entry| Ok(entry?.path()))
        .collect::<Result<_>>()?;

    cache.data = cache
        .data
        .into_iter()
        .filter(|(f, _)| entries.contains(f))
        .collect();

    let mtimes: HashMap<_, _> = cache
        .data
        .iter()
        .map(|(f, _)| Ok((f.clone(), f.metadata()?.modified()?)))
        .collect::<Result<_>>()?;

    cache.data = cache
        .data
        .into_iter()
        .filter(|(f, d)| d.mtime == mtimes[f])
        .collect();

    let new_data: Vec<_> = entries
        .into_par_iter()
        .filter(|f| !cache.data.contains_key(f))
        .filter_map(|file| {
            let mtime = file.metadata().ok()?.modified().ok()?;
            let tags = read_tags(&file).ok()?;
            let image = open(&file)
                .ok()?
                .thumbnail(CONFIG.image_size, CONFIG.image_size);
            let data = image.to_rgba();
            Some((
                file,
                Data {
                    mtime,
                    tags,
                    size: data.dimensions(),
                    thumbnail: data.into_raw(),
                },
            ))
        })
        .collect();
    cache.data.extend(new_data);

    let mut file = cache_dir().ok_or("Couldn't find cache directory")?;
    file.push("gdimp_cache");
    File::create(file)?.write_all(&serialize(&cache)?)?;

    Ok(cache)
}
