use crate::{config::CONFIG, vertex::Vertex, Result};
use glium::{
    index::{NoIndices, PrimitiveType::TriangleStrip},
    uniform, Display, Program, Surface, VertexBuffer,
};

pub struct Cursor {
    pub position: (u32, u32),
    f_position: (f32, f32),

    vertex_buffer: VertexBuffer<Vertex>,
    indices: NoIndices,
    program: Program,
}

impl Cursor {
    pub fn new(display: &Display) -> Result<Cursor> {
        Ok(Cursor {
            position: (0, 0),
            f_position: (0.0, 0.0),
            vertex_buffer: VertexBuffer::new(
                display,
                &[
                    Vertex::new(-1.0, -1.0, 0.0, 0.0),
                    Vertex::new(-1.0, 1.0, 0.0, 1.0),
                    Vertex::new(1.0, -1.0, 1.0, 0.0),
                    Vertex::new(1.0, 1.0, 1.0, 1.0),
                ],
            )?,
            indices: NoIndices(TriangleStrip),
            program: Program::from_source(
                display,
                include_str!("shaders/cell.vert"),
                include_str!("shaders/cursor.frag"),
                None,
            )?,
        })
    }

    pub fn draw(
        &mut self,
        target: &mut impl Surface,
        scroll: f32,
        offset: f32,
        image_size: (f32, f32),
        cell_size: (f32, f32),
    ) -> Result<()> {
        self.f_position = (
            self.f_position.0 + (self.position.0 as f32 - self.f_position.0) * 0.25,
            self.f_position.1 + (self.position.1 as f32 - self.f_position.1) * 0.25,
        );

        target.draw(
            &self.vertex_buffer,
            &self.indices,
            &self.program,
            &uniform! {
                position: if CONFIG.animation { self.f_position } else { (self.position.0 as f32, self.position.1 as f32) },
                scroll: scroll,
                offset: offset,
                image_size: image_size,
                cell_size: cell_size,
                cursor_width: CONFIG.cursor_width,
                cursor_color: CONFIG.cursor_color,
                background_color: CONFIG.background_color,
                pixel_size: 1.0 / CONFIG.image_size as f32,
            },
            &Default::default()
        )?;
        Ok(())
    }
}
