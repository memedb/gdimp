mod cache;
mod config;
mod cursor;
mod hex;
mod image;
mod layout;
mod output;
mod vertex;
mod window;

use crate::{cache::get_cache, config::CONFIG, window::Window};
use glium::glutin::event_loop::{ControlFlow, EventLoop};
use std::time::{Duration, Instant};

pub type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let (cache, reciever) = get_cache();

    let event_loop = EventLoop::new();
    let mut window = Window::new(cache?, &event_loop)?;

    event_loop.run(move |event, _, control_flow| {
        if CONFIG.animation {
            let next_frame_time = Instant::now() + Duration::from_nanos(16_666_667);
            *control_flow = ControlFlow::WaitUntil(next_frame_time);
        } else {
            *control_flow = ControlFlow::Wait;
        }

        if let Some(reciever) = &reciever {
            if let Ok(cache) = reciever.try_recv() {
                window.images_from_cache(cache).unwrap();
                window.calculate_layout();
                window.draw().unwrap();
            }
        }

        let should_draw = window.handle_event(event, control_flow).unwrap();
        if should_draw || CONFIG.animation {
            window.draw().unwrap();
        }
    });
}
