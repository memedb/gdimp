#version 140

in vec2 v_position;
in vec2 v_uv;
out vec2 uv;

// Per Image
uniform vec2 position;

// Global
uniform float scroll;
uniform float offset;
uniform vec2 cell_size;
uniform vec2 image_size;

// Reminder: coords go from bottom-left to top-right

void main() {
  uv = v_uv;
  gl_Position = vec4(
    vec2(-1.0, 1.0 - offset) + // Top left corner
    vec2(position.x, position.y - scroll) * cell_size * vec2(1.0, -1.0) + // Top left corner of cell
    cell_size * vec2(0.5, -0.5) + // Center of cell
    v_position * image_size // Image-sized square inside cell
  , 0.0, 1.0);
}
