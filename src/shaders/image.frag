#version 140

in vec2 uv;
out vec4 color;

const vec4 transparent = vec4(0, 0, 0, 0);

uniform sampler2D tex;
uniform vec2 ratio;
uniform bool selected;

uniform uint cursor_width;
uniform uint marker_size;
uniform vec4 marker_color;
uniform float pixel_size;

float square_mask(float origin, float size) {
  vec2 uv = vec2(uv.x, 1.0 - uv.y);
  vec2 a = step(origin, uv * ratio);
  vec2 b = 1.0 - step(size + origin, uv * ratio);
	return a.x * a.y * b.x * b.y;
}

void main() {
  color = texture(tex, uv);

  if (selected) {
    color = mix(
      color,
      transparent,
      square_mask(cursor_width * pixel_size, (float(marker_size) + 2.0) * pixel_size)
    );
    color = mix(
      color,
      marker_color,
      square_mask((float(cursor_width) + 1.0) * pixel_size, marker_size * pixel_size)
    );
  }
}
