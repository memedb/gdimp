#version 140

in vec2 uv;
out vec4 color;

const vec4 transparent = vec4(0, 0, 0, 0);

uniform uint cursor_width;
uniform vec4 cursor_color;
uniform vec4 background_color;
uniform float pixel_size;

float rectangle_mask(vec2 size) {
  vec2 bl = step(size, uv);
  vec2 tr = step(size, 1.0 - uv);
  return bl.x * bl.y * tr.x * tr.y;
}

void main() {
  if (rectangle_mask(vec2((float(cursor_width) + 1.0) * pixel_size)) > 0.0) {
    discard;
  }

  color = mix(
    cursor_color, background_color,
    rectangle_mask(vec2(float(cursor_width) * pixel_size))
  );
}
